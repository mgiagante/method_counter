require "method_counter/version"

module MethodCounter
  # TODO: Cómo hago que funcione con métodos que reciben parámetros?
  # Referencia: http://www.railstips.org/blog/archives/2009/05/15/include-vs-extend-in-ruby/

  module Countable
    module ClassMethods
      def count_invocations_of(*syms)
        syms.each do |sym|
          alias_method "original_#{sym}".to_sym, sym

          define_method(sym) do |*args|
            start_counter
            @invocations[sym] += 1
            send("original_#{sym}", *args)
          end
        end
      end
    end

    # Module#included hook
    def self.included(includer)
      includer.extend(ClassMethods)
    end

    def invoked?(sym)
      start_counter
      @invocations[sym] > 0
    end

    def times_invoked(sym)
      start_counter
      @invocations[sym]
    end

    private

    def start_counter
      @invocations ||= Hash.new(0)
    end
  end
end
